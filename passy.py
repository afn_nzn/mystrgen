import struct

def passModifier(pass0):
	sum = 0
	arr = []
	
	for i in pass0:
		sum += i * i
	for i in pass0:
		arr.append((sum + i * i) % 256)
	
	ret = b""
	for i in arr:
		ret += i.to_bytes(1, "little")
	return ret

def checkChara(code):
	if code >= ord("0") and code <= ord("9"):
		return True
	if code >= ord("a") and code <= ord("z"):
		return True
	if code >= ord("A") and code <= ord("Z"):
		return True
	
	return False

def getChIn62(code):
	tmp = code + 31
	while not checkChara(tmp):
		tmp = (tmp + 63) % 256
	return chr(tmp)

def create0(year, name):
	arr = []
	for i in name:
		arr.append(year * year * ord(i))
	str0 = b""
	for i in arr:
		str0 += struct.pack("I", i)
	return str0


def create1(pass0, str0):
	str1 = b""
	pass0 = passModifier(pass0)
	for i in range(0, len(str0)):
		str1 += bytes( (pass0[i % len(pass0)] ^ str0[i]).to_bytes(1, "little") )
		# print(bytes( (pass0[i % len(pass0)] ^ str0[i]).to_bytes(1, "little") ))
	return str1
	

def create2(str1, available_sigs):
	str2 = ""
	str2 += chr(str1[0] % 10 + ord('0'))
	str2 += chr(str1[0] % 26 + ord('a'))
	str2 += chr(str1[0] % 26 + ord('A'))
	
	if '+' in available_sigs:
		str2 += "+"
	if '-' in available_sigs:
		str2 += "-"
	if '_' in available_sigs:
		str2 += "_"
	if '@' in available_sigs:
		str2 += "@"
	
	return str2

def create3(str1):
	str3 = ""
	for i in str1:
		if checkChara(i):
			str3 += chr(i)
	return str3

def create4(str1):
	str4 = ""
	for i in str1:
		str4 += getChIn62(i)
	return str4

def createPass(pass0, pass1, pass2, pass3, year, signs, print_flag = False):
	a = create0(year, pass0)
	b0 = create1(pass0.encode(), a)
	b1 = create1(pass1.encode(), b0)
	b2 = create1(pass2.encode(), b1)
	b3 = create1(pass3.encode(), b2)
	d = create2(b3, signs)
	e = create3(b3)
	f = create4(b3)
	
	if print_flag:
		print("d : " + d)
		print("e : " + e)
		print("f : " + f)
		
	print(d + " " +  e + " " + f)
	return d + e + f

def createPass2(passArr, year, signs, print_flag = False):
	a = create0(year, passArr[0])
	length = len(passArr) - 1
	tmp = a
	for i in range(0, length):
		tmp = create1(passArr[i + 1].encode(), tmp)
	
	d = create2(tmp, signs)
	e = create3(tmp)
	f = create4(tmp)
	
	if print_flag:
		print("d : " + d)
		print("e : " + e)
		print("f : " + f)
		
	print(d + " " +  e + " " + f)
	return d + e + f


def printPass(s):
	rslt = ""
	length = len(s)
	for i in range(0, length // 5):
		rslt += s[i * 5: i * 5 + 5] + " "
	print(rslt)




